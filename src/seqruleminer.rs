//! Frequent (and naive) sequential rule miner.
//!


use std::{fmt, hash::{self, Hasher}, collections::{HashSet, HashMap}};

pub struct SequentialRule<T> {
    pub antecedent: Vec<T>,
    pub succedent: Vec<T>,
    pub support_set: Vec<usize>,
    pub confidence: f64,
    pub encoding_gain: f64,
    pub normalized_encoding_gain: f64
}

pub trait MDLEncoder<T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {

    fn fit(&mut self, all_rules: &Vec<SequentialRule<T>>) {
        unimplemented!()
    }

    fn rules_description_length(&self, working_rule_ids: &Vec<usize>, weights: &Vec<f64>, 
        all_rules: &Vec<SequentialRule<T>>, all_weights: &Vec<f64>) -> f64 {
        unimplemented!()
    }

    fn subsequence_code_length(&self, subsequence: &Vec<T>) -> f64 {
        unimplemented!()
    }

    fn rule_set_gain(&self, rule: &mut SequentialRule<T>) -> f64 {
        unimplemented!()
    }
}


impl<T> SequentialRule<T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    pub fn new(antecedent: Vec<T>, succedent: Vec<T>, support_set: Vec<usize>, confidence: f64) -> SequentialRule<T> {
        SequentialRule { 
            antecedent: antecedent, 
            succedent: succedent, 
            support_set: support_set, 
            confidence: confidence, 
            encoding_gain: 0.0,
            normalized_encoding_gain: 0.0
        }
    }

    pub fn new_partial_rule(antecedent: Vec<T>, succedent: Vec<T>) -> SequentialRule<T> {
        SequentialRule {
            antecedent: antecedent, 
            succedent: succedent, 
            support_set: Vec::new(),
            confidence: 0.0,
            encoding_gain: 0.0,
            normalized_encoding_gain: 0.0
        }
    }

    pub fn partial_clone(&self) -> SequentialRule<T> {
        SequentialRule {
            antecedent: Vec::from_iter(self.antecedent.iter().cloned()), 
            succedent: Vec::from_iter(self.succedent.iter().cloned()), 
            support_set: Vec::new(),
            confidence: self.confidence,
            encoding_gain: self.encoding_gain,
            normalized_encoding_gain: self.normalized_encoding_gain
        }
    }

    pub fn len(&self) -> usize {
        self.antecedent.len() + self.succedent.len()
    }

    pub fn as_sequence(&self) -> Vec<T>{
        let mut seq: Vec<T> = self.antecedent.clone();
        seq.extend(&self.succedent);
        seq
    }
}

impl<T> PartialEq for SequentialRule<T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    fn eq(&self, other: &Self) -> bool {
        self.antecedent == other.antecedent && self.succedent == other.succedent
    }
}

impl<T> Clone for SequentialRule<T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    fn clone(&self) -> Self {
        SequentialRule { antecedent: self.antecedent.clone(), 
            succedent: self.succedent.clone(), support_set: self.support_set.clone(), 
            confidence: self.confidence, encoding_gain: self.encoding_gain, 
            normalized_encoding_gain: self.normalized_encoding_gain
        }
    }
}

impl<T> Eq for SequentialRule<T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {}

impl<T> hash::Hash for SequentialRule<T> where  T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.antecedent.len().hash(state);
        for elem in &self.antecedent {
            elem.hash(state);
        }
        self.succedent.len().hash(state);
        for elem1 in &self.succedent {
            elem1.hash(state);
        }    
    }
}

impl<T> fmt::Display for SequentialRule<T> where T: fmt::Display {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut result : String = String::from("[");
        let antecedent_list : Vec<_> = self.antecedent.iter().map(|x| format!("{}", x)).collect();
        if antecedent_list.len() > 0{
            result.push_str(&antecedent_list.join(","));
        } else {
            result.push_str("∅ ");
        }
        
        result.push_str(" => ");
        let succedent_list : Vec<_> = self.succedent.iter().map(|x| format!("{}", x)).collect();            
        result.push_str(&succedent_list.join(","));
        result.push_str("]");
        write!(f, "{}", result)
    }
}

impl<T> fmt::Debug for SequentialRule<T> where T: fmt::Display {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut result : String = String::from("[");
        let antecedent_list : Vec<_> = self.antecedent.iter().map(|x| format!("{}", x)).collect();
        if antecedent_list.len() > 0{
            result.push_str(&antecedent_list.join(","));
        } else {
            result.push_str("∅ ");
        }
        
        result.push_str(" => ");
        let succedent_list : Vec<_> = self.succedent.iter().map(|x| format!("{}", x)).collect();            
        result.push_str(&succedent_list.join(","));
        result.push_str("]");
        result.push_str(&format!(", sup:{}, conf:{}, e-gain:{}, n-e-gain: {}", self.support_set.len(), self.confidence, self.encoding_gain, self.normalized_encoding_gain));
        write!(f, "{}", result)
    }
}
