mod seqminer;
mod seqruleminer;
mod cossu;

use crate::cossu::{NaiveRuleMiner, COSSU};
use crate::cossu::seqminer::{Sequence, Miner, SubsequenceMiner};
use std::time::Instant;
use std::{env, fs};

fn parse_precision(length_str: &String) -> u8  {
    let precision: u8 = match length_str.parse::<u8>() {
        Ok(n) => { 
            if n > 14 { eprintln!("Precision cannot be greater than 14, truncating to 14"); 14 } else { n } 
        },
        Err(e) => {
            eprintln!("The precision argument must be a positive integer smaller or equal than 14.");
            panic!("Exiting");
        }
    };

    precision
}

fn parse_support(min_supp_str: &String) -> u64  {
    let min_support: u64 = match min_supp_str.parse::<u64>() {
        Ok(n) => { 
            if n < 2 { eprintln!("The minimum support must be an integer greater or equal than 2. Taking default value 2"); 2 } else { n }
        },
        Err(e) => {
            eprintln!("The minimum support must be an integer");
            panic!("Exiting");
        }
    };

    min_support
}

fn parse_confidence(min_conf_str: &String) -> f64  {
    let min_conf: f64 = match min_conf_str.parse::<f64>() {
        Ok(c) => { 
            if c <= 0.0 || c > 1.0 { eprintln!("The minimum confidence is a value in the interval (0, 1]. Using 0.5"); 0.5 } else { c }
        },
        Err(e) => {
            eprintln!("The minimum confidence must be a real value in the interval (0, 1]");
            panic!("Exiting");
        }
    };

    min_conf
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let time_for_loading = Instant::now();
    let mut input_contents = String::new();
    if args.len() > 1 {
        input_contents = fs::read_to_string(&args[1]).expect(&format!("The program could not read the file at {}", args[1]));
    } 
    let mut tokens: Vec<u64> = vec![];
    for line in input_contents.lines() {
        let split = line.split(',');
        for elem in split {
            match elem.parse::<u64>() {
                Ok(n) => tokens.push(n),
                Err(e) => {}
            };
        }    
    }

    let time_elapsed_for_loading = time_for_loading.elapsed();
    eprintln!("Reading input sequence took {:?} seconds", time_elapsed_for_loading.as_secs_f64());
    let seq: Sequence<u64>  = Sequence::new(&tokens);
    eprintln!("The input sequence has {} tokens, and {} different symbols", tokens.len(), seq.alphabet.len());

    let mut min_supp = 2;
    if args.len() > 2 {
        min_supp = parse_support(&args[2]);
    }

    let mut min_conf = 0.5;
    if args.len() > 3 {
        min_conf = parse_confidence(&args[3]);
    }

    let mut precision: u8 = 5;
    if args.len() > 4 {
        precision = parse_precision(&args[4]);
    }
    
    // Sequence miner finds frequent subsequences
    let seq_miner = SubsequenceMiner::new(min_supp);    
    // The rule miner finds frequent sequential rules, e.g., A,C => B
    let rule_miner = NaiveRuleMiner::new(&seq_miner, precision, min_conf);
    // COSSU uses a rule miner to initialize its search space
    let cossu_miner: COSSU<u64> = COSSU::<u64>::new(&rule_miner, precision);
    let time_for_mining = Instant::now();
    let (rules, (code_length, weights)) = cossu_miner.mine(&seq);
    let time_elapsed_for_mining = time_for_mining.elapsed();
    eprintln!("Mining took {:?} seconds", time_elapsed_for_mining.as_secs_f64());

    // We output the rules and the weights
    for (idx, rule) in rules.iter().enumerate() {
        println!("{:?}, weight: {}", rule, weights[idx]);
    }
    eprintln!("Description length: {} bits", code_length);

}

