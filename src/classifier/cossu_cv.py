#!/usr/bin/env python
# coding: utf-8

# In[7]:


import numpy as np
from cossu import COSSUClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score
from joblib import Parallel, delayed


# In[8]:


folds_folder = "/home/lgalarra/cossu/data/folds"
data = {'classifiers': [], 'accuracy': [], 'precision': [], 'recall': []}
params = {'min_supp': 10, 'min_conf': 0.75, 'precision': 5, 'cossu_path': "/home/lgalarra/cossu/target/release"}

def train_and_test(idx, **args):
    classifier = COSSUClassifier(**args)
    classifier.train_from_files(folds_folder+'/train_without_gpa_with_dups_fold_'+str(idx), folds_folder+'/train_with_gpa_with_dups_fold_'+str(idx))
    with open(folds_folder+'/test_without_gpa_with_dups_fold_'+str(idx), "r") as fw:
        ln_without = len(fw.readlines())
    with open(folds_folder+'/test_without_gpa_with_dups_fold_'+str(idx), "r") as fw:
        ln_with = len(fw.readlines())        
    truth = np.concatenate((np.zeros(ln_without), np.ones(ln_with)))
    answers = np.array(classifier.predict_from_files(folds_folder+'/test_with_gpa_with_dups_fold_'+str(idx), folds_folder+'/test_with_gpa_with_dups_fold_'+str(idx)))
    acc = accuracy_score(truth, answers)
    prec = precision_score(truth, answers)
    rec = recall_score(truth, answers)
    print(acc, prec, rec)
    return classifier, acc, prec, rec

# In[ ]:

stop_points = [(0, 10)]
for sp, ed in stop_points:
    gen = Parallel(n_jobs=(sp - ed), prefer="processes")(delayed(train_and_test)(idx, **params) for idx in range(sp, ed))
    for cls, acc, prec, rec in gen:
        data['classifiers'].append(cls)
        data['accuracy'].append(acc)
        data['precision'].append(prec)
        data['recall'].append(rec)

print('Accuracy', data['accuracy'], np.mean(data['accuracy']), np.std(data['accuracy']))
print('Precision', data['precision'], np.mean(data['precision']), np.std(data['precision']))
print('Recall', data['recall'], np.mean(data['recall']), np.std(data['recall']))
# In[ ]:
