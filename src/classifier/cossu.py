import subprocess
import sys 
from os import path
from joblib import Parallel, delayed
import numpy as np

class COSSUClassifier(object):

    def __init__(self, min_supp=2, min_conf=0.5, precision=5, cossu_path=None):
        self.min_supp = min_supp
        self.min_conf = min_conf
        self.precision = precision
        self.cossu_path = cossu_path
        self.rule_files_ = []
        if self.cossu_path is not None:
            self.cossu_path = self.cossu_path.rstrip("/")
            self.dl_predictor = self.cossu_path + '/description-length'
            self.learner = self.cossu_path + '/cossu'

    @classmethod
    def from_rules(cls, *rule_files, **args):
        obj = cls(**args)
        obj.rule_files_ = []
        for r in rule_files:
            obj.rule_files_.append(r)
        obj.rules_ = [None for i in range(0, len(rule_files))]            
        return obj
    
    def train_from_files(self, *files):
        self.rules_ = [None for i in range(0, len(files))]
        self.rule_files_ = [None for i in range(0, len(files))]
        
        Parallel(n_jobs=len(files), prefer="processes")(
            delayed(self._mine_from_file)(file, idx) for idx, file in enumerate(files)
        )

        
    def _mine_from_file(self, file, idx):
        args = [self.learner, file, str(self.min_supp), str(self.min_conf), str(self.precision)]
        try:
            t = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                               timeout=None, text=True)
            self.rules_[idx] = t.stdout
            fname = 'rules_idx_' + str(idx) + path.basename(file)
            with open(fname, "w") as f:
                f.write(self.rules_[idx])
            self.rule_files_[idx] = fname
        except subprocess.TimeoutExpired as e:
            print(f'{e}', file=sys.stderr)
            raise e
        except Exception as e:
            print(f'Problem at mining.\n{e}', file=sys.stderr)
            raise e
        
        return True

    
    def dl(self, input_path, rules_file):
        args = [self.dl_predictor, input_path, rules_file, str(self.precision)]
        try:
            t = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               timeout=18000, text=True)
            dl = [float(v) for v in t.stdout.split("\n") if v != '']
        except subprocess.TimeoutExpired as e:
            print(f'{e}', file=sys.stderr)
            raise e
        except Exception as e:
            print(f'Problem during evaluation.\n{e}', file=sys.stderr)
            raise e
        
        return dl

        
    def predict_from_files(self, *input_paths):
        scores = []           
        for ifile in input_paths:
            file_scores = []
            for f in self.rule_files_:
                dl_list = self.dl(ifile, f)
                for idx, dl_value in enumerate(dl_list):
                    if idx >= len(file_scores):
                        file_scores.append([])
                    file_scores[idx].append(dl_value)
            scores.extend(file_scores)
        return np.array([s.index(min(s)) for s in scores])