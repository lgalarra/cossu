use std::{env, fs, fmt, hash};

#[path = "../cossu/mod.rs"]
mod cossu;

#[path = "../seqminer/mod.rs"]
mod seqminer;

use cossu::{seqminer::{Sequence, trie::Trie, SubsequenceMiner, Miner}, seqruleminer::{SequentialRule, MDLEncoder}, COSSUEncoder};

fn parse_u64_sequence(sequence_str: &str) -> Vec<u64>{
	let mut seq: Vec<u64> = vec![];
    for id_str in sequence_str.split(",") {
        if !id_str.contains("∅") {            
            seq.push(id_str.parse::<u64>().unwrap());
        }
    }
    seq
}

fn parse_rules(rules_str: &String) -> (Vec<SequentialRule<u64>>, Vec<f64>) {
    let mut parsed_rules: Vec<SequentialRule<u64>> = Vec::new();
    let mut parsed_weights: Vec<f64> = Vec::new();
    for rule_str in rules_str.lines() {
        if rule_str.starts_with("[") {
            let mut split = rule_str.split("],");
            
            let rule_str = split.next().unwrap();
            let weight_str = split.next().unwrap();
            let weight_value = weight_str.replace(" weight: ", "").parse::<f64>().expect("Error parsing a rule weight");        
            let s = rule_str.replace("[", "");
			let s1 = s.replace("]", "");
			let mut rule_split = s1.split(" => ");
            let antecedent = parse_u64_sequence(rule_split.next().unwrap());            
			let succedent = parse_u64_sequence(rule_split.next().unwrap());
			parsed_rules.push(SequentialRule::new_partial_rule(antecedent, succedent));
            parsed_weights.push(weight_value);
        }
    }

    (parsed_rules, parsed_weights)
}

fn compute_index<T: hash::Hash + Eq + Ord + Copy + fmt::Display>(seq: &Sequence<T>) -> Trie<T, Vec<usize>> {
    let seq_miner = SubsequenceMiner::new(2);
    let (_, index) = seq_miner.mine(seq);
    index
}

fn main() {
	let args: Vec<String> = env::args().collect();
    let mut input_contents = String::new();
    if args.len() > 1 {
        input_contents = fs::read_to_string(&args[1]).expect(&format!("The program could not read the sequence file at {}", args[1]));
    } 
    let mut seqs: Vec<Sequence<u64>> = vec![];
    for line in input_contents.lines() {
        let tokens: Vec<u64> = parse_u64_sequence(line);    
        let seq: Sequence<u64>  = Sequence::new(&tokens);
        seqs.push(seq);
    }


	let mut rules_content = String::new();
	if args.len() > 2 {
		rules_content = fs::read_to_string(&args[2]).expect(&format!("The program could not read the rules file at {}", args[1]));
	}

    let mut precision: u8 = 5;
    if args.len() > 3 {
        precision = args[3].parse::<u8>().unwrap();
    }

	let (mut rules, weights) = parse_rules(&rules_content);
    for seq in seqs {
        let index = compute_index::<u64>(&seq);
        for rule in rules.iter_mut() {
            rule.support_set = index.lookup_ref(&rule.as_sequence()).unwrap_or(&vec![]).clone();
        }
        let mut encoder = COSSUEncoder::new(&seq, precision, &index);
        encoder.fit(&rules);
        let dl = encoder.rules_description_length(&(0..rules.len()).collect(), &weights, &rules, &weights);
        println!("{}", dl);    
    }
	
}