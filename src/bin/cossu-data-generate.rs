use std::env;

#[path = "../cossu/mod.rs"]
mod cossu;

#[path = "../seqminer/mod.rs"]
mod seqminer;


use cossu::seqminer::Sequence;
use float_cmp::approx_eq;
use rand::Rng;

use crate::cossu::data_generator::DataGenerator;
use crate::cossu::seqruleminer::SequentialRule;

fn parse_length(length_str: &String) -> u64  {
    println!("{}", length_str);
    let length: u64 = match length_str.parse::<u64>() {
        Ok(n) => { n },
        Err(e) => {
            eprintln!("The first argument must be a positive integer.");
            panic!("Exiting");
        }
    };

    length
}

fn parse_rules(rules_str: &String) -> Vec<(SequentialRule<u16>, f64)> {
    let mut parsed_rules: Vec<(SequentialRule<u16>, f64)> = Vec::new();
    for rule_str in rules_str.split(',') {
        let mut split = rule_str.split(":");
        let ant_str = split.next().unwrap();
        let succ_str = split.next().unwrap();        
        let rule: SequentialRule<u16> = SequentialRule::new_partial_rule(
            ant_str.split("/").map(|x| x.parse::<u16>().unwrap()).collect::<Vec<u16>>(), 
            succ_str.split("/").map(|x| x.parse::<u16>().unwrap()).collect::<Vec<u16>>());
        parsed_rules.push((rule, split.next().unwrap().parse::<f64>().unwrap()));
    }

    parsed_rules
}

fn parse_alphabet(alphabet_str: &String) -> (Vec<u16>, Vec<f64>) {
    let mut tmp_probs: Vec<f64> = vec![];
    let mut tmp_alphabet: Vec<u16> = vec![];

    for c in alphabet_str.split(',') {
        let mut split = c.split(":");

        let elem: u16 = match split.next() {
            Some(v) => {  
                let p: u16 = match v.parse::<u16>() {
                    Ok(n) => { n },
                    Err(e) => { 
                        eprint!("Problems parsing the alphabet. 
                        It must be provided as a sequence elem1:probability1,elem2:probability2"); 
                        panic!("Exiting");
                    }
                };
                p
            },
            None =>  {
                eprint!(r#"Problems parsing the alphabet. 
                It must be provided as a sequence elem1:probability1,elem2:probability2"#);
                panic!("Exiting");
            }
        };
        
        let prob: f64 = match split.next() {
            Some(v) => { 
                let p = match v.parse::<f64>() {
                    Ok(n) => { n },
                    Err(e) => { 
                        eprint!("Problems parsing the alphabet. 
                        It must be provided as a sequence elem1:probability1,elem2:probability2"); 
                        panic!("Exiting");
                    }
                };
                p
            },
            None => { 
                eprint!("Problems parsing the alphabet. It must be provided as a sequence elem1:probability1,elem2:probability2"); 
                panic!("Exiting");
            }
        };
        tmp_probs.push(prob);
        tmp_alphabet.push(elem);
    }

    let total: f64 = tmp_probs.iter().sum();
    // This check should use some precision range
    if approx_eq!(f64, total, 1.0) {
        return (tmp_alphabet, tmp_probs);
    } else {
        eprintln!("The alphabet probabilities do not add up to 1 ({total}).");
        panic!("Exiting")
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut length: u64 = 5000;
    let mut alphabet: Vec<u16> = vec![0, 1, 2, 3, 4];
    let mut probs: Vec<f64> = vec![0.2, 0.2, 0.2, 0.2, 0.2];
    let mut rules_and_insertion_probs: Vec<(SequentialRule<u16>, f64)> = Vec::new();

    if args.len() > 1 {
        length = parse_length(&args[1]);
    }

    if args.len() > 2 {
        rules_and_insertion_probs.append(&mut parse_rules(&args[2]));
    }
    if rules_and_insertion_probs.len() > 0 {
        println!("Rules to insert:");
        for (rule, prob) in rules_and_insertion_probs.iter() {
            println!("{} with insertion probability {}", rule, prob);
        }
    }

    if args.len() > 3 {
        (alphabet, probs) = parse_alphabet(&args[3]);
    } else {
        println!("Assuming 5 elements with a uniform distribution.")
    }

    let mut rng = rand::thread_rng();
    let generator: DataGenerator<u16> = DataGenerator::new(rng.gen(), alphabet, probs);
    let sequence : Sequence<u16> = generator.generate_sequence(length, rules_and_insertion_probs);
    println!("{:?}", sequence);
    let _ = sequence.to_file("sequence.txt");
  

}