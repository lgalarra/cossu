// Trie implementation, taken from https://github.com/jmtuley/rust-trie/blob/master/src/lib.rs
// and extended to support iteration
use std::{hash::Hash, collections::HashMap};

pub struct Trie<K, V> where
    K: Eq + Hash + Clone,
    V: Clone,
{
    value: Option<V>,
    children: HashMap<K, Trie<K, V>>,
}

impl<K, V> Trie<K, V> where
    K: Eq + Hash + Clone,
    V: Clone,
{
    pub fn new() -> Trie<K, V> {
        Trie {
            value: None,
            children: HashMap::new(),
        }
    }

    pub fn push(&mut self, path: &Vec<K>, v: V) -> bool {
        self.push_from_i(path, 0, v)
    }

    fn push_from_i(&mut self, path: &Vec<K>, index: usize, v: V) -> bool {
        if index >= path.len() {
            return match self.value {
                Some(_) => false,
                None => {
                    self.value = Some(v);
                    true
                }
            }
        }

        self.children
            .entry(path[index].clone())
            .or_insert(Trie::new())
            .push_from_i(path, index+1, v)

    }

    pub fn lookup_as_mut_ref(&mut self, path: &Vec<K>) -> Option<&mut V> {
        match path.len() {
            0 => self.value.as_mut(),
            _ => self
                .children
                .get_mut(&path[0])
                .and_then(| child: &mut Trie<K, V> | child.lookup_as_mut_ref(&path[1..].to_vec())),
        }
    }

    pub fn lookup_ref(&self, path: &Vec<K>) -> Option<&V> {
        match path.len() {
            0 => self.value.as_ref(),
            _ => self
                .children
                .get(&path[0])
                .and_then(|child| child.lookup_ref(&path[1..].to_vec())),
        }
    }
}

impl<K, V> Iterator for Trie<K, V> where
    K: Eq + Hash + Clone,
    V: Clone, {
    type Item = (K, V);

    fn next(&mut self) -> Option<Self::Item> {
        unimplemented!()
    }
}