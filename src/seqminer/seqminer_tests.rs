#[cfg(test)] 
mod seqminer_tests {
    use crate::cossu::seqminer::{Sequence, SubsequenceMiner, Miner};

    #[test]
    fn seqminer_works() {
        let seq  = Sequence::new(&['D', 'E', 'C', 'D', 'E', 'A', 'D', 'E', 'B', 'D', 'E', 'D', 'E'].to_vec());
        println!("{}", &seq);
        let seq_miner = SubsequenceMiner::new(2);
        let (frequent_subsequences, _dictionary_subsequences) = seq_miner.mine(&seq);
        let value = _dictionary_subsequences.lookup_ref(&vec!['D', 'E']).unwrap();
        assert!(value.contains(&0) && value.contains(&3) && value.contains(&6) && value.contains(&11), "Sequence DE is in the trie!");
        for (sequence, _support_set) in frequent_subsequences {
            if sequence == vec!['D', 'E'] {
                assert!(true, "The sequence DE is there!");
                return
            }        
        }
    
        assert!(false, "The sequence DE is not there!");
    }
}