pub mod trie;

use std::collections::hash_map::DefaultHasher;
use std::fmt;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::hash::{self, Hash};
use std::hash::Hasher;
use std::io::{Write, Error};
use trie::Trie;

pub enum CustomError { InvalidAnswer }

pub type Subsequence<T> = (Vec<T>, Vec<usize>);
type UntranslatedSubsequence = Subsequence<u64>;

pub fn subsequence_vector_to_string<T: fmt::Display>(input: &Vec<Subsequence<T>>, with_occurrences : bool) -> String {
    let sequences_str_list : Vec<_> = input.iter().map(|x| subsequence_to_string(x, with_occurrences)).collect();
    format!("[{}]", sequences_str_list.join(","))
}

pub fn subsequence_to_string<T: fmt::Display>(input : &Subsequence<T>, with_occurrences : bool) -> String {
    let mut output : String = String::from("{sequence: {");
    let string_list : Vec<_> = input.0.iter().map(|x| format!("{}", x)).collect();
    output.push_str(&string_list.join(","));        
    output.push_str("}");
    if with_occurrences {
        output.push_str(", occurrences: {");
        let occurrences_list : Vec<_> = input.1.iter().map(|x| format!("{}", x)).collect();
        output.push_str(&occurrences_list.join(","));                    
        output.push_str("}");
    } 
    output.push_str("}");
    output
}

pub fn hash_subsequence<T: hash::Hash + Eq + Ord + Copy>(subsequence : &Vec<T>) -> u64 {
    let mut hasher = DefaultHasher::new();
    
    for elem in subsequence {
        elem.hash(&mut hasher);
    }

    hasher.finish()
}

// It represents an input sequence of arbitrary objects.
pub struct Sequence<T> {
    pub integer_seq: Vec<u64>,
    pub alphabet: Vec<T>,
    alphabet_mappings: HashMap<T, u64>,
    pub reverse_alphabet_mappings: HashMap<u64, T>,
    pub alphabet_frequencies : HashMap<T, u64>,
    element_position_map : HashMap<u64, Vec<usize>>    
}

pub trait Miner<T> where T: hash::Hash + Eq + Ord + Copy  {
    type PatternCollection;
    type AdditionalMiningInformation;

    fn mine(&self, input: &Sequence<T>) -> (Self::PatternCollection, Self::AdditionalMiningInformation);
}

impl<T> Sequence<T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    pub fn new(input_seq: &Vec<T>) -> Sequence<T> {
        let mut seq = Sequence {
            integer_seq : Vec::new(),
            alphabet : Vec::new(),
            alphabet_mappings : HashMap::new(),
            reverse_alphabet_mappings : HashMap::new(),
            alphabet_frequencies : HashMap::new(),
            element_position_map : HashMap::new()
        };
        seq.init_alphabet_dicts(input_seq);
        seq.init_position_map();
        seq.compute_alphabet_frequencies();
        seq
    }

    pub fn len(&self) -> usize {
        self.integer_seq.len()
    }

    fn init_alphabet_dicts(self : &mut Sequence<T>, original_seq : &Vec<T>) {
        let mut idcounter : u64 = 0;
        for elem in original_seq {
            match self.alphabet_mappings.get(elem) {
                Some(&value) => self.integer_seq.push(value),
                _ => { 
                    self.alphabet.push(*elem); 
                    self.integer_seq.push(idcounter); 
                    self.alphabet_mappings.entry(*elem).or_insert(idcounter);                        
                    idcounter += 1;  
                }
            }

            self.reverse_alphabet_mappings.insert(*self.alphabet_mappings.get(elem).unwrap(), *elem);
        }
    }

    fn init_position_map(self : &mut Sequence<T>) {
        for (i, elem) in self.integer_seq.iter().enumerate() {
            if !self.element_position_map.contains_key(elem) {
                self.element_position_map.insert(*elem, Vec::new());
            }
            match self.element_position_map.get_mut(elem) {
                Some(value) => value.push(i),
                _ => {}
            }
        }
    }

    fn compute_alphabet_frequencies(self : &mut Sequence<T>) {
        // Build the first level patterns
        for elem in &self.alphabet {
            match self.alphabet_mappings.get(&elem) {
                Some(value) => {
                    match self.element_position_map.get(&value) {
                        Some(support_set) => {self.alphabet_frequencies.insert(*elem, support_set.len() as u64);}
                        _ => {}
                    };
                }
                _ => {}
            }
        }
    }


    fn map_subsequence_to_integer_space(&self, subsequence: &Vec<T>) -> Result<Vec<u64>, CustomError> {
        if subsequence.iter().any(|x| self.alphabet_mappings.get(x).is_none()) {
            Err(CustomError::InvalidAnswer)
        } else {
            Ok(subsequence.iter().map(|x| self.alphabet_mappings.get(x).unwrap().clone()).collect::<Vec<u64>>())
        }
    }

    fn map_subsequence_to_original_space(&self, subsequence: &Vec<u64>) -> Result<Vec<T>, CustomError> {
        if subsequence.iter().any(|x| self.reverse_alphabet_mappings.get(x).is_none()) {
            Err(CustomError::InvalidAnswer)
        } else {
            Ok(subsequence.iter().map(|x| self.reverse_alphabet_mappings.get(x).unwrap().clone()).collect::<Vec<T>>())
        }
    }

    pub fn map_element_to_original_space(&self, int_elem: u64) -> Result<T, CustomError> {
        match self.reverse_alphabet_mappings.get(&int_elem) {
            Some(elem) => {
                Ok(*elem)
            },
            _ => {
                Err(CustomError::InvalidAnswer)
            }
        }
    }

    pub fn count(&self, subsequence: &Vec<T>) -> u64 {
        match self.map_subsequence_to_integer_space(subsequence) {
            Ok(int_subsequence) => {
                let mut count = 0;
                for i in 0..self.integer_seq.len() {
                    let mut k = 0;
                    let mut j = i;
                    while self.integer_seq[j] == int_subsequence[k] && k < int_subsequence.len() {
                        j += 1;
                        k += 1;
                    }
                    if k == subsequence.len() {
                        count += 1;
                    }
                }

                count    
            },
            Err(_) => {
                0
            }
        }

    }

    pub fn to_file(&self, outfile: &str) -> Result<usize, Error> {
        let mut file = File::create(outfile)?;
        let csv : String = self.to_csv();
        let data: &[u8] = csv.as_bytes();
        file.write_all(data)?;
        Ok(data.len())
    }

    pub fn to_csv(&self) -> String {
        let mut tokens: Vec<String> = vec![];
        for ielem in &self.integer_seq {
            let elem: &T = self.reverse_alphabet_mappings.get(&ielem).unwrap();
            tokens.push(format!("{}", elem));
            
        }

        tokens.join(",")
    }

    pub fn get_occurrences_element(&self, elem: &T) -> Vec<usize>{
        let elemid: &u64 = self.alphabet_mappings.get(&elem).unwrap();
        self.element_position_map.get(elemid).unwrap().clone()
    }
        
}

impl<T> fmt::Display for Sequence<T> where T: fmt::Display {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut result : String = String::from("[");
        if self.integer_seq.len() <= 10 {
            for elem in &self.integer_seq {
                result.push_str(&self.alphabet[*elem as usize].to_string());                    
            }
        } else {
            for i in 0..5 {
                result.push_str(&self.alphabet[self.integer_seq[i] as usize].to_string());
            }                
            result.push_str("...");
            let n = self.integer_seq.len();                
            for i in n - 6..n - 1 {
                result.push_str(&self.alphabet[self.integer_seq[i] as usize].to_string());
            }
        }
        result.push_str("]");
        write!(f, "{}", result)
    }
}

impl<T> fmt::Debug for Sequence<T> where T: fmt::Display {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut result : Vec<String> = vec![];
        for elem in &self.integer_seq {
            result.push(String::from(self.alphabet[*elem as usize].to_string()));                    
        }
        write!(f, "[{}]", result.join(","))
    }
}

pub struct SubsequencePartialSet {
    pub patterns_list: Vec<Subsequence<u64>>,
    levels : HashMap<u8, Vec<usize>>,
    children_to_parents : HashMap<usize, usize>

}

pub struct SubsequenceMiner {
    min_support : u64,
}

impl SubsequenceMiner {
    pub fn new(minsup : u64) -> SubsequenceMiner {
        SubsequenceMiner {
            min_support : minsup,
        }
    }

    fn translate<T : hash::Hash + Eq + Ord + Copy + fmt::Display>(&self, subsequences_indexes: &Vec<usize>, 
        subsequences : &Vec<UntranslatedSubsequence>,
        input_sequence: &Sequence<T>) -> Vec<Subsequence<T>> {
        let mut translated_subsequences : Vec<Subsequence<T>> = Vec::new();
        for subsequence_idx in subsequences_indexes {
            let (pattern, support_set) = &subsequences[*subsequence_idx as usize];
            let mut new_pattern : Vec<T> =  Vec::new();
            let mut new_support_set : Vec<usize> = Vec::new();
            for u64_elem in pattern {
                new_pattern.push(*input_sequence.reverse_alphabet_mappings.get(&u64_elem).unwrap());                    
            }
            // I need to find a way to make a move here.
            new_support_set.extend(support_set);
            let result_tuple = (new_pattern, new_support_set);
            translated_subsequences.push(result_tuple);
        }
        translated_subsequences
    }

}


impl<T> Miner<T> for SubsequenceMiner where T : hash::Hash + Eq + Ord + Copy + fmt::Display {
    type PatternCollection = Vec<Subsequence<T>>;
    type AdditionalMiningInformation = Trie<T, Vec<usize>>;

    fn mine(&self, input: &Sequence<T>) -> (Vec<Subsequence<T>>, Trie<T, Vec<usize>>){
        // We have to initialize the position index: elem => vector of positions
        let elempositionmap = input.element_position_map.iter().filter(|(_k, v)| (v.len() as u64) >= self.min_support).map(|(k, v)| (k.clone(), v.clone())).collect();
        let mut result = SubsequencePartialSet::new();
        let mut subsequences_dictionary: Trie<T, Vec<usize>> = Trie::new();

        result.init(&elempositionmap, &input, &mut subsequences_dictionary);

        // Start the mining
        let mut level : u8 = 2;
        let mut n_new_patterns : u64 = result.patterns_list.len() as u64;

        while n_new_patterns > 0 {
            //Extend the patterns at the current level
            n_new_patterns = result.extend(level, self.min_support, &input, &mut subsequences_dictionary);
            level += 1;                
        }
        // We return the list of sequences and sort of dictionary with the precomputed support
        let closed_translated_subsequences: Vec<Subsequence<T>> = self.translate::<T>(&result.closed_subsequences(), &result.patterns_list, input);
        
        (closed_translated_subsequences, subsequences_dictionary)
    }

}


impl SubsequencePartialSet {
    pub fn new() -> SubsequencePartialSet {
        SubsequencePartialSet {
            patterns_list : Vec::new(),
            levels : HashMap::new(),
            children_to_parents : HashMap::new()
        }
    }

    pub fn init<T : hash::Hash + Eq + Ord + Copy + fmt::Display>(&mut self, elements_to_positions : &HashMap<u64, Vec<usize>>, 
        input_seq: &Sequence<T>, subsequences_dictionary : &mut Trie<T, Vec<usize>>) {
        let mut levels_vec : Vec<usize> = Vec::new();
        self.patterns_list.push((vec![], (0..(input_seq.len() - 1)).collect()));
        subsequences_dictionary.push(&vec![], (0..(input_seq.len() - 1)).collect());

        
        for (elem, support_set) in elements_to_positions.iter() {
            self.patterns_list.push(
                (vec![*elem], support_set.clone())
            );
            subsequences_dictionary.push(&vec![input_seq.map_element_to_original_space(*elem).ok().unwrap()], 
                support_set.clone()
            );

            let pattern_id: usize = self.patterns_list.len() - 1;
            levels_vec.push(pattern_id);
            self.children_to_parents.insert(pattern_id, 0);
        }
        self.levels.insert(0 as u8, vec![0]);
        self.levels.insert(1 as u8, levels_vec);            
    }

    pub fn extend<T : hash::Hash + Eq + Ord + Copy + fmt::Display>(&mut self, level : u8, min_support: u64, input_sequence: &Sequence<T>, 
        subsequences_dict: &mut Trie<T, Vec<usize>>) -> u64 {
        let mut new_patterns : Vec<Subsequence<u64>> = Vec::new();
        let mut new_patterns_positions : Vec<usize> = Vec::new();
        let n_patterns = self.patterns_list.len();

        match self.levels.get(&(level - 1)) {
            Some(value) => {
                for idx in value {
                    let mut new_support_map : HashMap<u64, Vec<usize>> = HashMap::new(); 
                    {
                        let (pattern, support_set) = &self.patterns_list[*idx as usize];
                        let pattern_length = pattern.len();
                        for pattern_occurrence_idx in support_set {
                            let next_element_idx = (pattern_occurrence_idx + pattern_length) as usize;

                            if next_element_idx < input_sequence.len() {
                                let values = new_support_map.entry(input_sequence.integer_seq[next_element_idx]).or_insert(Vec::new());     
                                values.push(*pattern_occurrence_idx);          
                            }
                        }
                        new_support_map.retain(|_k, _v| _v.len() >= min_support as usize);
                        // Now construct the new subsequences
                    }

                    let old_sequence = &self.patterns_list[*idx as usize]; 
                    for (new_elem, new_support_set) in &new_support_map {
                        let pos_pattern = n_patterns + new_patterns.len();
                        new_patterns_positions.push(pos_pattern);
                        new_patterns.push(
                            self.new_subsequence(old_sequence, *new_elem, new_support_set)
                        );
                        self.children_to_parents.insert(pos_pattern, *idx);
                    } 
                }
            },
            _ => {}
        }
        let n_new_patterns = new_patterns.len();
        self.levels.insert(level, new_patterns_positions);
        for (int_pattern, support_set) in new_patterns.iter() {
            subsequences_dict.push(
                &input_sequence.map_subsequence_to_original_space(int_pattern).ok().unwrap(), 
                support_set.clone()
            );
        }
        (&mut self.patterns_list).append(&mut new_patterns);

        n_new_patterns as u64
    }

    fn new_subsequence(&self, subsequence: &UntranslatedSubsequence, new_item: u64, matches: &Vec<usize>) -> UntranslatedSubsequence {
        let mut new_sequence : Vec<u64> = Vec::new();
        let mut new_matches : Vec<usize> = Vec::new();
        let (old_sequence, _) = subsequence;
        for elem in old_sequence {
            new_sequence.push(*elem);
        }
        new_sequence.push(new_item);

        for new_match in matches {
            new_matches.push(*new_match)
        }

        (new_sequence, new_matches)
    }

    pub fn closed_subsequences(&self) -> Vec<usize> {
        let mut result: Vec<usize> = Vec::new();
        let mut excluded_patterns: HashSet<usize> = HashSet::new();
        // Look for patterns with different support than their parent
        for level in (0..self.levels.len()).rev() {
            for current_pattern_id in self.levels.get(&(level as u8)).unwrap().iter() {
                let (_pattern, result_set) = &self.patterns_list[*current_pattern_id];
                match self.children_to_parents.get(current_pattern_id) {
                    Some(parent_id) => {
                        let (_parent_pattern, parent_support_set) = &self.patterns_list[*parent_id];
                        if !excluded_patterns.contains(current_pattern_id) {
                            result.push(*current_pattern_id);
                        }
                        if parent_support_set.len() == result_set.len() {                         
                            excluded_patterns.insert(*parent_id);
                        }
                    },
                    _ => {
                        if !excluded_patterns.contains(current_pattern_id) {
                            result.push(*current_pattern_id);
                        }

                    }
                }

            }                
        }

        result
    }

    fn get_index_sequence_to_subsequence<T : hash::Hash + Eq + Ord + Copy + fmt::Display>(&self, input: &Sequence<T>) -> Vec<Vec<usize>> {
        let mut result_index = vec![vec![];input.len()]; 
        for (pattern_idx, (int_pattern, support_set)) in self.patterns_list.iter().enumerate() {
            for start_idx in support_set {
                let end_idx = start_idx + int_pattern.len();
                if end_idx <= input.len() {
                    result_index[end_idx].push(pattern_idx);
                }
            }
        }
        result_index
    }

}    

impl fmt::Display for SubsequencePartialSet  {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut result : String = String::from("[");
        for (i, (patt, occurrences)) in self.patterns_list.iter().enumerate() {
            if i < 5 || i >= self.patterns_list.len() - 5 {
                result.push_str(&patt.iter().map(|x| x.to_string()).collect::<Vec<String>>().join(", "));
                result.push_str(&format!("{}", i));
            } else if i == 5 {
                result.push_str("...");
            } 
        }
        result.push_str("]");
        write!(f, "{}", result)
    }
}

