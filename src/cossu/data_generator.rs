use std::{hash, fmt, collections::HashMap};

use rand::{Rng, rngs::StdRng, SeedableRng};
use rand_distr::{Uniform, Distribution};

use crate::cossu::{seqruleminer::SequentialRule, seqminer::Sequence};

pub struct DataGenerator<T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display  {
    pub random_seed : u64,
    pub alphabet: Vec<T>,
    pub alphabet_probs: Vec<f64>
}

impl<T> DataGenerator<T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {

    pub fn new(random_seed: u64, alphabet: Vec<T>, alphabet_probs: Vec<f64>) -> DataGenerator<T> {
        DataGenerator{
            random_seed : random_seed,
            alphabet: alphabet,
            alphabet_probs: alphabet_probs
        }
    }

    pub fn generate_sequence(&self, length: u64, rules_and_insertion_probs: Vec<(SequentialRule<T>, f64)>) -> Sequence<T> {
        let uniform = Uniform::from(0..self.alphabet.len());
        let mut rng = StdRng::seed_from_u64(self.random_seed);
        let mut sequence: Vec<T> = Vec::new();
        let mut active_rules: HashMap<SequentialRule<T>, i8> = HashMap::new();
        for (rule, _insertion_prob) in rules_and_insertion_probs.iter() {
            active_rules.insert(rule.clone(), -1 as i8);
        }

        for _i in 0..length {
            let elem = self.alphabet[uniform.sample(&mut rng)];
            sequence.push(elem);
            for (rule, _insertion_prob) in rules_and_insertion_probs.iter() {
                let last_matched_idx = active_rules.get(&rule).unwrap();
                let match_idx = last_matched_idx + 1;
                if match_idx < rule.antecedent.len() as i8 {
                    if elem == rule.antecedent[match_idx as usize] {
                        active_rules.entry(rule.clone()).and_modify(|k| *k = match_idx);
                        // Bingo!
                        if match_idx == (rule.antecedent.len() as i8) - 1 {
                            let throw = rng.gen::<f64>();
                            if throw <= *_insertion_prob {   
                                for new_elem in rule.succedent.iter() {
                                    sequence.push(*new_elem);
                                }
                            }
                            active_rules.entry(rule.clone()).and_modify(|k| *k = -1);
                        }
                    } else {
                        active_rules.entry(rule.clone()).and_modify(|k| *k = -1);
                    }
                }
            }
        }

        Sequence::new(&sequence)
    }
}