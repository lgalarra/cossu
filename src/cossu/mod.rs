pub mod data_generator;
pub mod cossu_tests;

#[path="../seqminer/mod.rs"]
pub(crate) mod seqminer;
#[path="../seqruleminer.rs"]
pub(crate) mod seqruleminer;
#[path="../seqminer/seqminer_tests.rs"]
pub(crate) mod seqminer_tests;

use std::{hash, fmt, collections::{HashSet, HashMap}, process::exit};
use rand_distr::num_traits::Pow;
use seqminer::{SubsequenceMiner, Sequence, Subsequence, Miner};
use seqruleminer::{SequentialRule, MDLEncoder};
use seqminer::trie::Trie;


pub struct NaiveRuleMiner<'a> {
    sequence_miner : &'a SubsequenceMiner,
    pub weights_precision: u8,
    pub min_confidence : f64,
}

impl<'a> NaiveRuleMiner<'a> {
    pub fn new(miner : &'a SubsequenceMiner, precision: u8, min_confidence : f64) -> NaiveRuleMiner<'a> {
        NaiveRuleMiner {
            sequence_miner: miner,
            weights_precision: precision,
            min_confidence: min_confidence            
        }
    }

    pub fn mine_new_rule_from_subsequence<T : hash::Hash + Eq + Ord + Copy + fmt::Display>(&self, subseq : &Subsequence<T>, 
        input_seq : &Sequence<T>, count_state : &Trie<T, Vec<usize>>, 
        seen_rules: &mut HashSet<SequentialRule<T>>, encoder: &COSSUEncoder<'_, T>) -> Option<SequentialRule<T>> {
        let mut max_gain = f64::NEG_INFINITY;
        let mut best_rule: Option<SequentialRule<T>> = None;
        if subseq.0.len() == 0 {
            return best_rule
        }
        // Given that the sequences are unique, no need to double check this rule
        // for existence
        // Look at the rule ∅ => sequence, but avoid singletons
        if subseq.0.len() > 1 {
            let conf = (subseq.1.len() as f64) / (input_seq.len() as f64);
            if conf >= self.min_confidence {
                let mut empty_antecedent_rule : SequentialRule<T> = SequentialRule::new(vec![], // antecedent
                    Vec::from_iter(subseq.0.iter().cloned()), // consequent
                    Vec::from_iter(subseq.1.iter().cloned().map(|x| x as usize)), // support set
                    conf // confidence
                ); 
                encoder.rule_set_gain(&mut empty_antecedent_rule);
                if empty_antecedent_rule.encoding_gain > max_gain {
                    max_gain = empty_antecedent_rule.encoding_gain;
                    best_rule = Some(empty_antecedent_rule.partial_clone());
                }

                seen_rules.insert(empty_antecedent_rule);
            }
        }

        // Look at every split point
        for i in 1..subseq.0.len() {
            // Check if the rule has been already mined
            let antecedent = subseq.0[0..i].to_vec();
            let succedent = subseq.0[i..subseq.0.len()].to_vec();
            let partial_rule = SequentialRule::new_partial_rule(antecedent, succedent); 
            match seen_rules.get(&partial_rule) {
                Some(_value) => {}
                _ => {
                    let support_antecedent = count_state.lookup_ref(&partial_rule.antecedent).unwrap().len() as f64;
                    let confidence : f64 = (subseq.1.len() as f64) / support_antecedent;
                    if confidence >= self.min_confidence {
                        let mut new_rule = SequentialRule::new(partial_rule.antecedent.iter().cloned().collect(), 
                        partial_rule.succedent.iter().cloned().collect(), 
                            Vec::from_iter(subseq.1.iter().cloned().map(|x| x as usize)), confidence);
                        encoder.rule_set_gain(&mut new_rule);
                        if new_rule.encoding_gain > max_gain {
                            max_gain = new_rule.encoding_gain;
                            best_rule = Some(new_rule.partial_clone());
                        }
                        seen_rules.insert(new_rule);         
                    }
                }
            }
        }

        best_rule

    }

    fn normalize_encoding_gain<T : hash::Hash + Eq + Ord + Copy + fmt::Display>(&self, rules: &mut Vec<SequentialRule<T>>, factor: f64) {
        for rule in rules {
            rule.normalized_encoding_gain = rule.encoding_gain / factor; 
        }
    }

    fn get_best_encoding_gain_parent_rule<T : hash::Hash + Eq + Ord + Copy + fmt::Display>(&self, sequence: &Vec<T>, 
        antecedent_length: usize, rules_trie: &Trie<T, f64>) -> f64 {
        let mut max_encoding_gain = f64::NEG_INFINITY;
        for i in 1..antecedent_length {
            match rules_trie.lookup_ref(&sequence[i..].to_vec()) {
                Some(v) => {
                    if *v > max_encoding_gain {
                        max_encoding_gain = *v;
                    }
                },
                None => { },
            }
        }   
        max_encoding_gain
    }

}

impl<T> Miner<T> for NaiveRuleMiner<'_> where T : hash::Hash + Eq + Ord + Copy + fmt::Display {
    type PatternCollection = Vec<SequentialRule<T>>;
    type AdditionalMiningInformation = (Vec<usize>, Trie<T, Vec<usize>>);

    fn mine(&self, input : &Sequence<T>) -> (Vec<SequentialRule<T>>, (Vec<usize>, Trie<T, Vec<usize>>)) {
        let (frequent_closed_subsequences, subsequences_dictionary) = self.sequence_miner.mine(input);
        let mut seen_rules : HashSet<SequentialRule<T>> = HashSet::new();
        let encoder = COSSUEncoder::new(input, self.weights_precision, &subsequences_dictionary);
        // We will now extract association rules from each frequent subsequence
        let n_subsequences = frequent_closed_subsequences.len();
        if n_subsequences > 1 {
            let mut rules: Vec<SequentialRule<T>> = vec![];
            let mut rules_trie: Trie<T, f64> = Trie::new(); // This structure assumes each subsequence can produce at most 1 rule
            for subsequence in frequent_closed_subsequences[1..n_subsequences].iter().rev() {
                match NaiveRuleMiner::<'_>::mine_new_rule_from_subsequence(&self, &subsequence, input, 
                    &subsequences_dictionary, &mut seen_rules, &encoder) {
                    Some(mut partial_rule) => { 
                        if partial_rule.encoding_gain > 0.0 {
                            let best_parent_encoding = self.get_best_encoding_gain_parent_rule(&subsequence.0, 
                                partial_rule.antecedent.len(), &rules_trie);
                            if best_parent_encoding < partial_rule.encoding_gain { 
                                rules_trie.push(&subsequence.0, partial_rule.encoding_gain);
                                let full_rule = seen_rules.get(&partial_rule).unwrap();
                                partial_rule.support_set = Vec::from_iter(full_rule.support_set.iter().cloned());
                                rules.push(partial_rule);
                            }
                        } 
                    }
                    None => {}
                }
            }
            let mut sorted_indexed_rules : Vec<(usize, &SequentialRule<T>)> = rules.iter().enumerate().collect::<Vec<(usize, &SequentialRule<T>)>>();
            sorted_indexed_rules.sort_by(|r, rp| rp.1.encoding_gain.partial_cmp(&r.1.encoding_gain).unwrap());
            let rule_indexes_sorted_by_gain = sorted_indexed_rules.iter().map(|(idx, _v)| *idx).collect::<Vec<usize>>();
            if rule_indexes_sorted_by_gain.len() > 0 {
                // Let us normalize the gain
                let max_gain = rules[rule_indexes_sorted_by_gain[0]].encoding_gain;
                self.normalize_encoding_gain(&mut rules, max_gain);
            }
            (rules, (rule_indexes_sorted_by_gain, subsequences_dictionary))
        } else {
            (vec![], (vec![], subsequences_dictionary))
        }
    }

}


fn phi() -> f64 {
    let five: f64 = 5.0;
    (five.sqrt() + 1.0) / 2.0
}

fn truncate(f: f64, precision: u8) -> f64 {
    let factor: f64 = 10.0_f64.pow(precision);
    f64::trunc(f  * factor) / factor
}

pub struct COSSUEncoder<'a, T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    training_sequence: &'a Sequence<T>,
    weights_precision: u8,
    subsequences_to_positions_in_sequence: &'a Trie<T, Vec<usize>>,
    pub positions_to_predicting_rules: Vec<HashSet<usize>>,
    positions_to_triggering_but_not_predicting_rules: Vec<HashSet<usize>>,
    predicting_rules_to_positions: Vec<Vec<usize>>,
    triggering_but_not_predicting_rules_to_positions: Vec<Vec<usize>>

}

pub struct COSSU<'a, T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    rule_miner : &'a dyn Miner<T, PatternCollection=Vec<SequentialRule<T>>, AdditionalMiningInformation=(Vec<usize>, Trie<T, Vec<usize>>)>,
    weights_precision: u8
}

impl<'a, T> COSSU<'a, T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    pub fn new(rule_miner : &'a dyn Miner<T, PatternCollection=Vec<SequentialRule<T>>, AdditionalMiningInformation=(Vec<usize>, Trie<T, Vec<usize>>)>, precision: u8) -> COSSU<'a, T> {
        COSSU {
            rule_miner : rule_miner,            
            weights_precision: precision
        }
    }

    fn get_singleton_rules(&self, long_seq: &Sequence<T>) -> Vec<SequentialRule<T>> {
        let mut result: Vec<SequentialRule<T>> = Vec::new();
        for elem in long_seq.alphabet.iter() {
            let support_set: Vec<usize>  = long_seq.get_occurrences_element(elem);
            let conf : f64 = (support_set.len() as f64) / (long_seq.len() as f64);
            let support_set_size = support_set.len(); 
            result.push(SequentialRule{
                antecedent: vec![],
                succedent: vec![*elem],
                support_set: support_set,
                confidence:  conf,
                encoding_gain: (support_set_size.pow(2) as f64) / (long_seq.len() as f64),
                normalized_encoding_gain: 0.0
            })
        }

        result
    }


    fn gss(&self, weight_position: usize, working_rule_ids: &Vec<usize>, target_weights: &mut Vec<f64>, 
        all_rules: &Vec<SequentialRule<T>>, all_weights: &Vec<f64>, encoder: &COSSUEncoder<T>) {
        let tol = encoder.min_weight();

        let mut a = 0.0 + tol;
        let target_rule = &all_rules[working_rule_ids[weight_position]];
        let mut b = if target_rule.normalized_encoding_gain <= 0.0 || target_rule.normalized_encoding_gain >= 1.0  { 1.0 - tol } else { target_rule.normalized_encoding_gain };
        let phi = phi();

        let mut c: f64 = b - (b - a) / phi;
        let mut d: f64 = a + (b - a) / phi;

        while (b - a).abs() > tol {
            target_weights[weight_position] = truncate(c, self.weights_precision);
            //let f_c = encoder.rules_description_length(working_rule_ids, &target_weights, all_rules, all_weights);
            let (f_cr, fc_d) = encoder.single_rule_description_length(weight_position, working_rule_ids, target_weights, all_rules, all_weights);
            let f_c = f_cr + fc_d;
            target_weights[weight_position] = truncate(d, self.weights_precision);
            let (f_dr, fd_d) = encoder.single_rule_description_length(weight_position, working_rule_ids, target_weights, all_rules, all_weights);
            let f_d = f_dr + fd_d;
            //let f_d = encoder.rules_description_length(working_rule_ids, &target_weights, all_rules, all_weights);
            if f_c < f_d {
                b = d;
            } else {
                a = c;
            }

            c = b - (b - a) / phi;
            d = a + (b - a) / phi;
        }

        target_weights[weight_position] = truncate((b + a) / 2.0, self.weights_precision);

    }

    fn adjust_weights(&self, idx_working_rules: &Vec<usize>, target_weights: &Vec<f64>, 
        all_rules: &Vec<SequentialRule<T>>, all_weights: &Vec<f64>, start_target_weight_id: usize,
        encoder: &COSSUEncoder<T>, lazy_mode: bool) -> Vec<f64> {
        // Optimize each weight independently
        let mut new_weights: Vec<f64> = target_weights.clone();
        let mut all_weights_copy = all_weights.clone();
        for idx in (start_target_weight_id..target_weights.len()).rev(){
            if lazy_mode {
                let succ_a = all_rules[idx].succedent.iter().collect::<HashSet<_>>();
                let succ_b = all_rules[target_weights.len() - 1].succedent.iter().collect::<HashSet<_>>();
                let intersection = succ_a.intersection(&succ_b).collect::<Vec<_>>();    
                if intersection.len() > 0 {
                    self.gss(idx, idx_working_rules, &mut new_weights, all_rules, &all_weights_copy, encoder);
                    self.update_weights(&vec![idx_working_rules[idx]], &new_weights, &mut all_weights_copy);
                }
            } else {
                self.gss(idx, idx_working_rules, &mut new_weights, all_rules, &all_weights_copy, encoder);
                self.update_weights(&vec![idx_working_rules[idx]], &new_weights, &mut all_weights_copy);
            }
        }

        new_weights
    }

    fn update_weights(&self, ids: &Vec<usize>, new_weights: &Vec<f64>, all_weights: &mut Vec<f64>) {
        for (idx_new_weights, idx_all_weights) in ids.iter().enumerate() {
            all_weights[*idx_all_weights] = new_weights[idx_new_weights];
        }
    } 


}

impl<'a, T> COSSUEncoder<'a, T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    pub fn new(input_sequence : &'a Sequence<T>, precision: u8, subsequences_to_sequence_positions: &'a Trie<T, Vec<usize>>) -> COSSUEncoder<'a, T> {
        COSSUEncoder { 
            training_sequence: input_sequence, 
            weights_precision: precision, 
            subsequences_to_positions_in_sequence: subsequences_to_sequence_positions,
            positions_to_predicting_rules: vec![HashSet::new();input_sequence.len()],
            positions_to_triggering_but_not_predicting_rules: vec![HashSet::new();input_sequence.len()],
            predicting_rules_to_positions: vec![],
            triggering_but_not_predicting_rules_to_positions: vec![]
        }
    }

    fn min_weight(&self) -> f64 {
        (10.0_f64).powi(-(self.weights_precision as i32))
    }

    fn sequence_code_length_given_rules(&self, working_rule_ids: &Vec<usize>, weights: &Vec<f64>, global_to_local_rule_ids: &HashMap<usize, usize>,  all_weights: &Vec<f64>) -> f64 {
        // Iterate over each character in the sequence and get the rules that trigger + those that predict
        let mut description_length_data_given_rules = 0.0;
        let working_rule_ids_set: HashSet<usize> = HashSet::from_iter(working_rule_ids.iter().cloned());
        for (elem_idx, _int_elem) in self.training_sequence.integer_seq.iter().enumerate() {
            let mut weights_rules_matched = self.min_weight(); 
            let mut weights_rules_triggered = self.min_weight();
            let predicting_rules_ids = &self.positions_to_predicting_rules[elem_idx];
            
            for rule_idx in working_rule_ids_set.intersection(predicting_rules_ids) {
                match global_to_local_rule_ids.get(rule_idx) {
                    Some(local_id) => { 
                        weights_rules_matched += weights[*local_id]; 
                        weights_rules_triggered += all_weights[*rule_idx];
                    },
                    None => { 
                        weights_rules_matched += all_weights[*rule_idx]; 
                        weights_rules_triggered += all_weights[*rule_idx];                        
                    },
                }
            }                    


            // Now check the rules whose antecendents end here
            // First check the subsequences that match here
            let triggered_rules_ids = &self.positions_to_triggering_but_not_predicting_rules[elem_idx];
            for triggered_rules_id in working_rule_ids_set.intersection(triggered_rules_ids) {
                match global_to_local_rule_ids.get(triggered_rules_id) {
                    Some(local_id) => { weights_rules_triggered += weights[*local_id]; },
                    None => { weights_rules_triggered += all_weights[*triggered_rules_id]; },
                }                
            }
            
            description_length_data_given_rules += -(weights_rules_matched / weights_rules_triggered).log2();
        }
        description_length_data_given_rules
    }

    fn rules_code_length(&self, working_rule_ids: &Vec<usize>, weights: &Vec<f64>, all_rules: &Vec<SequentialRule<T>>) -> (f64, HashMap<usize, usize>) {
        let mut description_length_for_rules = (1.0 + working_rule_ids.len() as f64).log2();
        // L(R)
        let mut global_to_local_rule_ids : HashMap<usize, usize> = HashMap::new();
        for (idx, rule_idx) in working_rule_ids.iter().enumerate() {            
            let weight = weights[idx];                 
            let rule_description_length = self.single_rule_code_length(*rule_idx, weight, all_rules);
            description_length_for_rules += rule_description_length;
            global_to_local_rule_ids.insert(*rule_idx, idx);

        }
        (description_length_for_rules, global_to_local_rule_ids)
    }

    fn single_rule_code_length(&self, rule_idx: usize, weight_to_encode: f64, all_rules: &Vec<SequentialRule<T>>) -> f64 {
        let rule: &SequentialRule<T> = &all_rules[rule_idx];
        let mut rule_description_length = 0.0;
        rule_description_length += self.subsequence_code_length(&rule.antecedent) + 1.0;
        rule_description_length += self.subsequence_code_length(&rule.succedent) + 1.0;
        let mut weight = weight_to_encode;                 
        for i in 0..self.weights_precision {
            let v = weight * 10.0_f64.powf((i + 1).into());
            let digit = v as u32;
            rule_description_length += ((digit + 1) as f64).log2();
            weight -= (digit as f64) / 10.0_f64.powf((i + 1).into());
        }

        rule_description_length
    }

    fn sequence_code_length_given_single_rule(&self, target_rule_idx: usize, working_rule_ids: &Vec<usize>, all_weights: &Vec<f64>) -> f64 {
        let mut description_length_data_given_rules = 0.0;
        let working_rule_ids_set: HashSet<usize> = HashSet::from_iter(working_rule_ids.iter().cloned());

        for elem_idx in &self.predicting_rules_to_positions[target_rule_idx] {
            let weights_rules_matched = all_weights[target_rule_idx];
            let mut weights_rules_triggered = all_weights[target_rule_idx];

            let triggering_rules_ids = &self.positions_to_triggering_but_not_predicting_rules[*elem_idx];
            
            for rule_idx in triggering_rules_ids.intersection(&working_rule_ids_set) {
                weights_rules_triggered += all_weights[*rule_idx];                        
            }

            description_length_data_given_rules += -(weights_rules_matched / weights_rules_triggered).log2();                    
        }

        for elem_idx in &self.triggering_but_not_predicting_rules_to_positions[target_rule_idx] {
            let mut weights_rules_matched = 0.0;
            let mut weights_rules_triggered = 0.0;

            let matching_rules_ids = &self.positions_to_predicting_rules[*elem_idx];
            for matching_rule_id in matching_rules_ids.intersection(&working_rule_ids_set) {
                weights_rules_matched += all_weights[*matching_rule_id];
                weights_rules_triggered += all_weights[*matching_rule_id];
            }

            let triggered_rules_ids = &self.positions_to_triggering_but_not_predicting_rules[*elem_idx];
            for triggered_rules_id in triggered_rules_ids.intersection(&working_rule_ids_set) {
                weights_rules_triggered += all_weights[*triggered_rules_id];
            }
            description_length_data_given_rules += -(weights_rules_matched / weights_rules_triggered).log2();
        }
        
        
        description_length_data_given_rules
    }



    fn single_rule_description_length(&self, working_rules_ids_local_id: usize, working_rule_ids: &Vec<usize>, target_weights: &Vec<f64>, 
        all_rules: &Vec<SequentialRule<T>>, all_weights: &Vec<f64>) -> (f64, f64) {
        (
            self.single_rule_code_length(working_rule_ids[working_rules_ids_local_id], target_weights[working_rules_ids_local_id], all_rules),
            self.sequence_code_length_given_single_rule(working_rule_ids[working_rules_ids_local_id], working_rule_ids, all_weights)
        )
        
    }

}

impl<T> MDLEncoder<T> for COSSUEncoder<'_, T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    
    fn fit(&mut self, all_rules: &Vec<SequentialRule<T>>) {
        self.predicting_rules_to_positions.resize(all_rules.len(), vec![]);
        self.triggering_but_not_predicting_rules_to_positions.resize(all_rules.len(), vec![]);

        // Populate the indexes position -> rules
        for (rule_idx, rule) in all_rules.iter().enumerate() {
            for seq_idx in &rule.support_set {
                let end_antecedent_idx = seq_idx + rule.antecedent.len();
                for k in 0..rule.succedent.len() {
                    let seq_idx_final = end_antecedent_idx + k;
                    if seq_idx_final < self.training_sequence.len() {
                        self.positions_to_predicting_rules[seq_idx_final].insert(rule_idx);
                        self.predicting_rules_to_positions[rule_idx].push(seq_idx_final);
                    }
                }
            }
        }

        for (rule_idx, rule) in all_rules.iter().enumerate() {
            // Now get the antecedent of the rule and do the same
            match self.subsequences_to_positions_in_sequence.lookup_ref(&rule.antecedent) {
                Some(positions) => {
                    for starting_pos in positions {
                        let end_antecedent_idx = *starting_pos + rule.antecedent.len();
                        for k in 0..rule.succedent.len() {
                            let pos = end_antecedent_idx + k;
                            if pos < self.training_sequence.len() {
                                if !self.positions_to_predicting_rules[pos].contains(&rule_idx) {
                                    self.positions_to_triggering_but_not_predicting_rules[pos].insert(rule_idx);
                                    self.triggering_but_not_predicting_rules_to_positions[rule_idx].push(pos);
                                }
                            }
                        }
                    }
                }
                None => {},
            }                
        }

    }


    fn rules_description_length(&self, working_rule_ids: &Vec<usize>, weights: &Vec<f64>, all_rules: &Vec<SequentialRule<T>>, all_weights: &Vec<f64>) -> f64 {
        // L(R)
        let (description_length_for_rules, global_to_local_rule_ids) = self.rules_code_length(working_rule_ids, 
            weights, all_rules);
        // L(S|R)
        let description_length_data_given_rules = self.sequence_code_length_given_rules(working_rule_ids, weights, 
            &global_to_local_rule_ids, all_weights);
        description_length_for_rules + description_length_data_given_rules
    }

    fn subsequence_code_length(&self, sequence: &Vec<T>) -> f64 {
        let seq_len = if sequence.len() <= 1 { sequence.len() + 1 } else { sequence.len() };
        let mut slength_code_length = (seq_len as f64).log2();

        for elem in sequence {
            let cardinality : u64 = *self.training_sequence.alphabet_frequencies.get(elem).unwrap_or(&0);
            if cardinality > 0 {
                slength_code_length += -((cardinality as f64) / (self.training_sequence.len() as f64)).log2();
            } else {
                slength_code_length += -self.min_weight().log2();
            }
        }

        slength_code_length
    }


    fn rule_set_gain(&self, rule: &mut SequentialRule<T>) -> f64 {
        rule.encoding_gain = rule.confidence * (rule.support_set.len() as f64) * self.subsequence_code_length(&rule.succedent) 
        - (self.subsequence_code_length(&rule.antecedent) + self.subsequence_code_length(&rule.succedent));
        rule.encoding_gain
    }
}

impl<T> Miner<T> for COSSU<'_, T> where T: hash::Hash + Eq + Ord + Copy + fmt::Display {
    type PatternCollection = Vec<SequentialRule<T>>;
    type AdditionalMiningInformation = (f64, Vec<f64>); // Return the code length and the rule weights
    
    fn mine(&self, input: &Sequence<T>) -> (Vec<SequentialRule<T>>, (f64, Vec<f64>)) {
        let (mut mined_rules, (iteration_order, subsequences_dictionary)) = self.rule_miner.mine(input);
        // Now construct the singleton rules.
        let mut all_rules: Vec<SequentialRule<T>> = self.get_singleton_rules(input);
        let n_singleton_rules = all_rules.len();
        all_rules.append(&mut mined_rules);
        let mut encoder = COSSUEncoder::new(input, self.weights_precision, &subsequences_dictionary);
        encoder.fit(&all_rules);
        
        let n_all_rules = all_rules.len();
        let initial_weight = 1.0 - 10.0_f64.powf(-(encoder.weights_precision as f64));
        let mut all_weights = vec![initial_weight;n_all_rules];
        let mut adjusted_weights = vec![initial_weight;n_singleton_rules];

        // Adjust the weights
        let mut working_rule_set_ids : Vec<usize> = (0..n_singleton_rules).collect::<Vec<usize>>();
        adjusted_weights = self.adjust_weights(&working_rule_set_ids, &adjusted_weights, &all_rules, &all_weights, 0, &encoder, false);
        self.update_weights(&working_rule_set_ids, &adjusted_weights, &mut all_weights);
        let mut previous_weights = adjusted_weights.clone();
        let mut best_code_length : f64 = encoder.rules_description_length(&working_rule_set_ids, &adjusted_weights, &all_rules, &all_weights);
        let mut idx_accepted_rules : Vec<usize> = working_rule_set_ids.clone();

        for old_idx in iteration_order {
            let idx = old_idx + n_singleton_rules;
            working_rule_set_ids.push(idx);
            previous_weights.push(all_weights[idx]);
            assert!(working_rule_set_ids.len() == previous_weights.len());
            adjusted_weights = self.adjust_weights(&working_rule_set_ids, &previous_weights, &all_rules, &all_weights, n_singleton_rules, &encoder, false);            
            // For speed up, we will only optimize the weights of the recently added rule
            self.update_weights(&working_rule_set_ids, &adjusted_weights, &mut all_weights);
            let mut current_code_length = encoder.rules_description_length(&working_rule_set_ids, &adjusted_weights, &all_rules, &all_weights);
            if current_code_length < best_code_length {
                idx_accepted_rules.push(idx);

                // Accept the rule but test if other rules became dispensable     
                let mut tmp_candidate_rule_set_ids : Vec<usize> = Vec::new();
                let mut tmp_weights: Vec<f64> = Vec::new(); 

                // Reformulate this loop to implement lines 8-10 in the algorithm
                let candidate_rules_to_exclude = idx_accepted_rules.iter().filter(|x| **x >= n_singleton_rules && **x != idx).cloned().collect::<Vec<usize>>();
                let mut excluded_rules_ids: HashSet<usize> = HashSet::new();
                for rule_idx_to_exclude in candidate_rules_to_exclude.iter() {
                    tmp_weights.append(&mut idx_accepted_rules.iter()
                    .filter(|retain_rule_idx| **retain_rule_idx != *rule_idx_to_exclude && !excluded_rules_ids.contains(rule_idx_to_exclude))
                    .map(|x| all_weights[*x]).collect());
                    
                    tmp_candidate_rule_set_ids.append(&mut idx_accepted_rules.iter()
                    .filter(|retain_rule_idx| **retain_rule_idx != *rule_idx_to_exclude && !excluded_rules_ids.contains(rule_idx_to_exclude))
                    .cloned().collect());

                    assert!(tmp_candidate_rule_set_ids.len() == tmp_weights.len());

                    let delete_code_length = encoder.rules_description_length(&tmp_candidate_rule_set_ids, &tmp_weights, &all_rules, &all_weights);
                    if delete_code_length <= current_code_length {
                        current_code_length = delete_code_length;
                        excluded_rules_ids.insert(*rule_idx_to_exclude);
                        // Update everything
                        idx_accepted_rules = tmp_candidate_rule_set_ids.clone();
                        previous_weights = tmp_weights.clone();
                        working_rule_set_ids = idx_accepted_rules.clone();

                    }
                    tmp_weights.clear();
                    tmp_candidate_rule_set_ids.clear();
                }
                best_code_length = current_code_length;   
            } else {
                // Undo everything
                for (id, rule_idx) in working_rule_set_ids.iter().enumerate() {
                    all_weights[*rule_idx] = previous_weights[id];
                }
                working_rule_set_ids.pop();
                previous_weights.pop();
            }
        }

        let mut accepted_rules: Vec<SequentialRule<T>> = Vec::new();
        for idx_accepted_rule in idx_accepted_rules {
            let accepted_rule = &all_rules[idx_accepted_rule];
            accepted_rules.push(accepted_rule.clone());
        }

        (accepted_rules, (best_code_length, adjusted_weights))
    }
}