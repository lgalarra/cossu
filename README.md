# COSSU

This is an implementation of the COSSU method presented in [Discovering Useful Compact Sets of Sequential Rules in a Long Sequence](https://arxiv.org/pdf/2109.07519.pdf). COSSU mines interesting sequential rules in long sequences using MDL.

## Compile the code

1. Install [Rust](https://www.rust-lang.org/tools/install)

`curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`

2. Clone this repository

3. Go to the cloned repository and compile the code with 

`cargo build` 

4. Compilation will generate two binaries: `cossu` and `cossu-data-generate`

## Use the code

The `cossu` binary expects an input file with a sequence of comma separated characters and optional minimal support and precision arguments (default values 2 and 5 respectively)

`cargo run --bin cossu INPUT_SEQUENCE [MIN_SUPPORT] [PRECISION]` (or just go to generated target/ directory and run the binary from there `cossu INPUT_SEQUENCE [MIN_SUPPORT] [PRECISION]`)

You can generate synthetic instances using the data generator:

`cargo run --bin cossu-data-generate SEQUENCE_LENGTH RULE1,RULE2,... [ALPHABET]`

A rule is provided as 3 components ANTECEDENT:SUCCEDENT:CONFIDENCE, e.g, 0/1:2:0.7 is interpreted as the sequential rule 0,1 => 2 with precision (confidence) 0.7.

For example the command:

`cargo run --bin cossu-data-generate 5000 0:1:0.5 0:0.1,1:0.1,2:0.1,3:0.1,4:0.1,5:0.1,6:0.1,7:0.1,8:0.1,9:0.1`

will generate a random sequence of 5000 random draws from the alphabet where the rule 0=>1 will be inserted with a probability of 0.5. Everytime a '0' is generated, a '1' will be inserted with 50% probability, which may lead to more than 5000 elements. The alphabet contains 10 equiprobable elements (with labels ranging from 0 to 9). If no alphabet is provided, we assume 5 equiprobable elements (0, 1, 2, 3, 4).
